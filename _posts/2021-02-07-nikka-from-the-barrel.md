---
layout: post
title: "Nikka - From the Barrel"
date:   2021-02-07 07:00:36 -0500
categories: Whisky
---

![Nikka From The Barrel - Tasting glass and bottle](/assets/posts/nikka-from-the-barrel.jpg)
After looking for this bottle for a long time, I was finally able to pick it up at a local grocery store. Even with a burnt tongue from cooking dinner, this whisky did not disappoint!

### Nose

The initial smell I got reminded me of walking into a pattiserie in Portugal, Pasteis de Belem. There were beautiful notes of vanilla, sugar cane, and a hint of lemon. It was also reminiscent of a creme brulee, creamy, caramel, and wonderful.

### Palate 

My tongue was not up for the challenge, so I'll be reviewing this one again in a couple of days. What I received in this tasting was a hint of smoke, as if you just snuffed out a campfire. Lots of vanilla and a hint of rose. I've found other Nikka whiskies to be extremely complex so I'm pretty disappointed in my palate tonight.

### Finish

The finish was delightful, with a bit of orange. Similar notes as the palate otherwise.

### Notes

I will be re-reviewing this in a couple of days because I know it has a lot more to give than what I got tonight.
