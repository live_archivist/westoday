---
layout: post
title: "Billy Strings - 2017-02-03 - Albuquerque, NM"
date:   2021-04-13 08:00:36 -0500
categories: 
  - Bootlegs
  - Must Listen
---

Following the poor quality of the first show I listened to today, it was a breath of fresh air to listen to a recording that was recorded and produced well. This was my first listen to Billy Strings and I can say I was definitely impressed. I'm normally not a big fan of Bluegrass but the sheer talent that this guy has had me hooked from the first hook of Sweet Blue Eyed Darlin.

I will definitely be on the lookout for more from Billy Strings in my archive! In the meantime check out this recording on the Internet Archive and don't forget to donate to them!

:star::star::star::star::star:

[BillyStrings2017-02-03 via Archive.org](https://archive.org/details/BillyStrings2017-02-03)