---
layout: post
title: "Nothing Left"
date:   2021-03-16 07:00:36 -0500
categories: Personal
---

I realized today as I stared at a simple document that I needed to update that I have nothing left to give. I have no bandwidth in my head for anything outside of my personal life right now. With the baby due any moment now, I cannot think about anything else. My initial response, the one conditioned in me for decades by the unhealthy obsession with work that the United States has, was to kick myself and push myself to get through it.

> "Just write the doc. It's not that hard." 

> "It'll only take you an hour."

I sat staring at the blinking cursor for at least 10 minutes, willing myself to just start typing - nothing came out. I even have the screenshots ready for the document, but no words come. Everything in my brain screams that I should have no issue writing this document, but my body says no. 

Instead I'm thinking about the amazing, life-changing event, that is going to be happening to us in the next few days. In less than a few days my wife will be giving birth to our first-born and we will become parents. I think about all the things that can go wrong first, then all the things that can go right. I think about the beauty of this process, how incredible it is that women's bodies are able to grow a temporary organ to support a fetus, how their physical structure changes to support giving birth. I think about the seriousness of bringing a baby into this world, something the baby didn't opt-in to, a world full of hate, racism, violence, but also love, beauty, and fun. 

I think about the obvious privilege that my child will be born into and what they'll choose to do with that privilege. I think about how I can shield my little one from this virus, even from family members who would choose conspiracy theories over science and the health of a newborn baby. I think about what kind of a parent I'll be, what kind of parent I'll want to be. 

But writing this document, one of the furthest things from my mind.

