---
layout: post
title: "Auto Dark Mode for this Website"
date:   2021-02-04 07:00:36 -0500
categories: Web
---

With more and more devices offering the ability to detect if the OS is in Dark Mode or Light Mode, I decided it was time to implement a non-javascript method of switching to a dark stylesheet for this website. It turns out it's pretty easy, so I'll walk you through it below.

``` css
@media (prefers-color-scheme: dark) {
    * {
    	$dark_text_color: #f6f6f6;
    	$dark_link_color: #4da3ff;
    	background-color: #171717; 
    	color: $dark_text_color;
        border-color: #e6e6e6;
    }
}
```

All you have to do is add a media query for `prefers-color-scheme: dark` and then put your dark mode styling in that code block. This method works so well that if a person is reading your website and their OS switches to dark mode, your website will immediately do the same! Pretty cool huh?
