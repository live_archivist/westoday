---
layout: post
title: "Bye Bye GSuite"
date:   2021-01-30 07:00:36 -0500
categories: Tech
---

Anyone who follows me on Twitter probably saw my semi-public spat with Google this week. Normally I don't bother with getting support from large companies like them, because it in general sucks, instead I either return the product to the retailer or deal with the issue. In this case, I was living with the fact that my Nest Thermostat has never reported the internal humidity of our house as anything other than 38%. This includes when we were adding on to our kitchen (where the thermostat is located) and had an entire wall open to the outside for two days. One of the days, it downpoured. 

For two years I've just gotten the internal humidity in our house from another sensor, no. big. deal. As I was relaying this point to someone on Twitter when they were asking about the Nest Thermostats, Google poked their head in and asked me to DM them for some troubleshooting. Normally when a company reaches out to help you without you asking, it's because they are actually willing to go above and beyond to fix the issue. So, I engaged.

We went back and forth over DMs for four days trying to troubleshoot the issue, often with the other side going dark for up to 12 hours at a time. The back and forth got very old so I asked them to send me a list of all the troubleshooting steps they wanted me to take so I could do them all at once. They promptly ignored my request and sent me a form to fill out asking for my serial number, shipping address, and Twitter handle. I filled out the form thinking they would be sending me a replacement thermostat. 

Two days after I filled out the form, they finally got back to me and asked me to fill out the form again. I refused, saying I had already done that two days prior. They checked and found the filled out form. Shortly after that they responded to say there was nothing that they will do because the thermostat is out of waranty BUT I could buy a new one in the Google Play store. They engaged me, wasted four days of my time, and then told me they couldn't do anything for me. A $1.275T company wouldn't replace a $275 thermostat after engaging me, who was seemingly okay with the limitations of the device, and then proceeded to waste 4 days of my time.

So, needless to say I was angry. So, I went about looking for ways to get rid of anything I have that gives Google money. First up, the Nest. I will be selling it and replacing with another device. Second, my personal GSuite account that I use for consulting. Third, about 15 seats of GSuite that my consulting clients use will be going to Office 365.

I surveyed Fastmail and Protonmail $5/mo plans for my personal use, here is how they broke down:

### Protonmail

* 5GB Storage
* 5 Addresses
* 1 Custom Domain
* Calendar, no sharing
* iOS Default Apps don't work
* End to End Encryption
* Hosted in Switzerland

### Fastmail

* 30GB Storage
* 600+ Addresses
* Multiple Custom Domains
* Calendar with Sharing
* iOS Default Apps work

For me, the biggest thing was the amount of storage and the multiple custom domain support. Having multiple domains that I use for various projects is useful and Fastmail doesn't charge me extra for something so simple. I like the Encryption and Hosting that Protonmail has, but it wasn't enough to push me over the edge for my consulting and project mail. I'll still continue to use Protonmail for my personal communication, but for projects, I've already moved everything over to Fastmail.

Oh, and Google, if you ever read this... Your support sucks.