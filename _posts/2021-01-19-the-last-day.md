---
layout: post
title:  "The Last Day"
date:   2021-01-19 07:00:36 -0500
categories: Politics
---

On the eve of Joe Biden's inauguration, I thought I would reflect on what the last four years have brought us. Four years of a Donald Trump presidency have sown deep chasms of division in politics, friendships, and families. In my immediate family, we are deeply divided in our political views, mainly my siblings and me vs. my parents, grandparents, and extended family.

We've had four years of lying and undermining our democracy, our news sources, and our ability to reason. Every news source bears some responsibility in this, but so do each of us. We retweet without reading, pass gossip and unsubstantiated facts to friends, and build an echo chamber so loud that we can't hear anything else. We've had four years of vilifying people for wanting not to be killed by an officer for a simple traffic stop or for writing a bad check. The rule of law is not happening in the courts but the streets.

We've had four years of saying that white nationalists are _"good people"_ and a most recent event where Trump, the President of the United States (for one more day!), incited insurrection then told the rioters gently that they should go home and _"we love you"_. Seriously. Those chasms of division sown before the insurrection have cause people to believe that the Capitol was stormed by Black Lives Matter protesters and Antifa rather than Trump's cult following bearing MAGA flags and QAnon garb. A not-insignificant number of people in this riot also were sporting white nationalist swag, and one was wearing a "Camp Auschwitz" shirt. But yes, these are _"good people"_. /s

We've watched as the country which claims to be the moral compass of the planet slowly erodes the rights of anyone who isn't white or wealthy. The right is fighting the left at every step because they dare to want healthcare for everyone, a living minimum wage, equality, equity for BIPOC, and for us to stop destroying our planet. For 12 years, they've been claiming _"they're gonna take our guns"_ when in fact, there have been no efforts made to increase background checks or even make it slightly more challenging to buy a weapon.

There's a lot of work ahead of us as a country. There's a lot of work ahead of us as friends and family members. At some point, we're going to have to put down our guns (literally and figuratively) and figure out how we can work together to make this country a safe and equitable place for everyone. 

We can't just sit and wait for those in power to start doing this; we have to make those strides individually, no matter how difficult the conversations get. So on the last day of Donald Trumps presidency, look at how you can make a change in your actions to help heal the wounds he's created.