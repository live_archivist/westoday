---
layout: post
title:  "Side Projects"
date:   2021-01-26 07:00:36 -0500
categories: Projects
---

I've always had lots of side projects to keep my creativity flowing. My wife thinks I'm crazy every time I set one of them aside to start another, but over time she's realized it's less about completing the projects for me and more about the process of creating. For instance, I've been trying to turn bourbon barrels into drums and building an electric go-kart for 2+ years now. I pick the project up for a while, run with it and then put it back down. 

Until recently I've not made much progress on the drums, but I actually have a rounded shell now. That shell is far from perfect, but I can't help but look at it fairly often as it sits on my office floor. My electric go-kart has been left to wallow in sorrow in our basement for far too long. After spending this past weekend cleaning up the basement, I decided it's time to start this one up again. So I'll be setting up the workbench so I can start welding up the battery packs.

Side Projects that are not related to my day job play an important role, allowing creativity to flow and stress to release as I focus on the project at hand. I guess some people would refer to these items as hobbies, but for me they're projects because the nature of the project changes often. This morning I started working on a Indoor Air Quality sensor platform, that I want to build. This is a project I had already prototyped some time last year but now I feel the drive to work on it more since we will have a newborn breathing the air inside our house soon.

Take some time to work on a side project unrelated to your dayjob. You're mental health will thank you.