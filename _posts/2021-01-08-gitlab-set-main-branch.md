---
layout: post
title:  "Gitlab Set Main Branch as Default"
date:   2021-01-08 12:00:36 -0500
categories: Gitlab
---

I've started getting rid of references to `master` branches in all of my repositories. It's rather easy with Gitlab, but I thought I would document the steps for anyone struggling with it.

Click `Repository -> Branches`:

![Repository then Branches](/assets/posts/gitlab-main-01.jpg)

Click `New Branch` and name it `main`:

![Create main Branch](/assets/posts/gitlab-main-02.jpg)

![name branch main](/assets/posts/gitlab-main-03.jpg)

Click `Settings -> Repository`:

![Settings Menu](/assets/posts/gitlab-main-04.jpg)

Expand `Default Branch` and choose `main`:

![Selecting main branch as default](/assets/posts/gitlab-main-05.jpg)

Scroll down and expand `Protect Branches`, protect `main` branch and allow `Maintainers` to merge and push.

![Protect main branch, allow maintainers to merge and push](/assets/posts/gitlab-main-06.jpg)

Click `Unprotect` next to the `master` branch:

![Unprotect the master branch](/assets/posts/gitlab-main-07.jpg)

Click `Repository -> Branches`:

![Repository Branches menu](/assets/posts/gitlab-main-01.jpg)

Delete "master" branch:

![Delete master branch](/assets/posts/gitlab-main-08.jpg)

That's all there is to it. I am disappointed that there doesn't seem to be a way to set this at my account level, maybe they'll do that in the future.