---
layout: default
---

{%- if site.posts.size > 0 -%}
<div class="posts">
    <ul>
        {% for post in site.posts %}
                <li><a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a></li>
        {% endfor %}
    </ul>
</div>
{% endif %}