---
layout: default
---

# Open Compensation

I firmly believe in the sharing of compensation. 

Compensation disparity between the sexes and races have been caused by the idea that talking about salary and overall compensation is taboo. Below you'll see the titles I've held and the rough timelines through my compensation changes.

## Current: 

- **Senior Tech Marketing Engineer** - $205k starting raised to $215k, ~$25k/yr in stock

## Previous:

- **Senior Sales Engineer** - $194k OTE 70:30 salary:commission, Small stock grants
- **Sales Engineer** - $160k OTE 70:30 salary:commission starting, raised to $176k, ~$200k Options over 4 years
- **Lead Architect** - $55k (massively underpaid)
- **Virtualization Engineer** - $45k starting, raised to $50k
- **Senior Desktop Analyst** - $42k
- **Desktop Analyst - $15/hr** 