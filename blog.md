---
layout: default
---

{%- if site.posts.size > 0 -%}
<div class="posts">
    <ul>
        {% for post in site.posts %}
            {% if post.categories contains 'Bootlegs' %}
            {% else %}
                <li><a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a></li>
            {% endif %}
        {% endfor %}
    </ul>
</div>
{% endif %}